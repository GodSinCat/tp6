# tp6

#### 介绍
该项目使用thinkphp6开发

#### 功能模块

用户模块
·支持手机号+验证码登录
·通过消息队列发送短信验证码
·分布式session解决方案
分类管理
·支持无限极分类
·利用Tp6调试功能处理分类管理中存在的一些场景问题
商品模块
·商品抢购能力支持
·利用Tp6缓存机制处理商品首页API逻辑数据
·利用TP6错误+日志模块定位系统存在的问题
·商品高级sku管理
支付模块
·支持微信、支付宝支付
·支付模块服务化

#### 项目框架



#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

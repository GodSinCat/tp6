<?php

use think\facade\Route;

Route::rule("smscode", "sms/code", "POST");
Route::rule("login", "login/index", "GET");

Route::resource('user', 'User');
Route::rule("lists", "mall.lists/index");
Route::rule("subcategory/:id", "category/sub");
Route::rule("detail/:id", "mall.detail/index");

Route::resource("order", "order.index");
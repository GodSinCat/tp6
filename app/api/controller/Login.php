<?php

declare (strict_types = 1);
namespace app\api\controller;

use app\BaseController;
use app\common\business\User;

class Login extends BaseController {

    public function index() {
        if (!$this->request->isPost()) {
            return show(config("status.error"), "非法请求");
        }

        //获取登录信息
        $phone = $this->request->param("phone_number", "", "trim");
        $code  = input("param.code", 0, "intval");
        $type  = input("param.type", 0, "intval");

        //组装登录数据到数组
        $data = [
            'phone_number' => $phone,
            'code'         => $code,
            'type'         => $type,
        ];

        //校验数据
        $validate = new \app\api\validate\User();
        if (!$validate->scene("login")->check($data)) {
            return show(config("status.error"), $validate->getError());
        }

        //调用逻辑层数据
        try {
            $result = (new User())->login($data);
        } catch (\Exception $e) {
            return show($e->getCode(), $e->getMessage());
        }

        if ($result) {
            return show(config("status.success"), "登录成功", $result);
        }
        return show(config('status.error'), "登录失败");

    }
}

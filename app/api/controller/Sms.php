<?php

declare (strict_type = 1);
namespace app\api\controller;
use app\BaseController;
use app\common\business\Sms as SmsBus;

class Sms extends BaseController {

    public function code(): object{
        $phoneNumber = input('param.phone_number', '', 'trim');

        $data = [
            'phone_number' => $phoneNumber,
        ];
        try {
            validate(\app\api\validate\User::class)->scene("send_code")->check($data);
        } catch (\think\exception\ValidateException $e) {
            // 验证失败 输出错误信息
            return show(config("status.error"), $e->getError());
        }
        if (SmsBus::sendCode($phoneNumber, 4, "ali")) {
            return show(config('status.success'), '发送验证码成功');
        }
        return show(config('status.error'), '发送验证码失败');

    }

}
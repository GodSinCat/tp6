<?php
declare (strict_types = 1);
namespace app\common\business;
use app\common\lib\ClassArr;
use app\common\lib\Num;
use app\common\lib\sms\AliSms;

class Sms {

    public static function sendCode(string $phoneNumber, int $len, string $type = "ali"): bool{

        $code = Num::getNumber($len);

        //工厂模式  发射机制
        $classsStats = ClassArr::smsClassStat();

        $classObj = ClassArr::initClass($type, $classsStats);

        $sms = $classObj::sendCode($phoneNumber, $code);
        if ($sms) {
            cache(config('redis.code_pre') . $phoneNumber, $code, config('redis.code_expire'));

        }
        return $sms;
    }

}
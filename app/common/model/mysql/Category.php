<?php

namespace app\common\model\mysql;
use think\Model;

class Category extends Model {

    public function getLists($data, $num = 10) {
        $order = [
            "listorder" => "desc",
            "id"        => "desc",
        ];
        $result = $this->where("status", "<>", config("status.mysql.table_delete"))
            ->where($data)
            ->order($order)
            ->paginate($num);
        //echo $this->getLastSql();exit;
        return $result;
    }

    /**
     * getChildCountInPids 带同学们解读
     * @param $condition
     * @return mixed
     */
    public function getChildCountInPids($condition) {
        $where[] = ["pid", "in", $condition['pid']];
        $where[] = ["status", "<>", config("status.mysql.table_delete")];
        $res     = $this->where($where)
            ->field(["pid", "count(*) as count"])
            ->group("pid")
            ->select();
        //echo $this->getLastSql();exit;
        return $res;
    }

    public function getNormalCategorys($field = "*") {
        $where = [
            "status" => config("status.mysql.table_normal"),
        ];

        $order = [
            "listorder" => "desc",
            "id"        => "desc",
        ];
        $result = $this->where($where)
            ->field($field)
            ->order($order)
            ->select();

        return $result;
    }

    public function getNormalByPid($pid = 0, $field) {
        $where = [
            'pid'    => $pid,
            'status' => config('status.mysql.table_normal'),
        ];

        $order = [
            "listorder" => "desc",
            "id"        => "desc",
        ];

        $res = $this->where($where)
            ->field($field)
            ->order($order)
            ->select();
        return $res;
    }
}
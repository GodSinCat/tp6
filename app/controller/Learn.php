<?php
/**
 * Created by PhpStorm.
 * User: 29639
 * Date: 2020/5/5
 * Time: 12:58
 */

namespace app\controller;


use app\Request;
use \think\facade\Request as Abc;
class Learn
{
    public function index(Request $request){
        //2
        dump($request->param("abc","intval"));
        //3
        dump(input("abc"));
        //4
        dump(request()->param("abc"));
        //5
        dump(Abc::param("abc",1,"intval"));
    }
}
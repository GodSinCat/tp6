<?php

namespace app\admin\controller;

use app\BaseController;
use think\exception\HttpResponseException;

class AdminBase extends BaseController {

    public $adminUser = null;
    public function initialize() {
        parent::initialize();
        if (empty($this->isLogin())) {
            return $this->redirect(url('/admin/login/index'), '302');
        }
    }

    /**
     * 判断是否登录
     * @Author   cat
     * @DateTime 2020-06-03T23:26:02+0800
     * @return   boolean                  [description]
     */
    public function isLogin() {
        $this->adminUser = session(config('admin.session_admin'));
        if (empty($this->adminUser)) {
            return false;
        }
        return true;
    }

    public function redirect(...$args) {
        throw new HttpResponseException(redirect(...$args));
    }
}
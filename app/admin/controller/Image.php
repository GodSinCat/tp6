<?php

namespace app\admin\controller;

class Image extends AdminBase {

    public function upload() {

        if (!$this->request->isPost()) {
            return show(config('status.error'), '请求不合法');
        }
        $file     = $this->request->file('file');
        $filename = \think\facade\Filesystem::disk('public')->putFile('image', $file);
        // $filename = \think\facade\Filesystem::disk('public')->putFile("image", $file);

        if (!$filename) {
            return show(config('status.error'), '上传图片失败');
        }

        $imgUrl = [
            'image' => '/upload/' . $filename,
        ];

        return show(config('status.success'), '上传成功', $imgUrl);

    }

    public function layUpload() {
        if (!$this->request->isPost()) {
            return show(config("status.error"), "请求不合法");
        }
        $file     = $this->request->file("file");
        $filename = \think\facade\Filesystem::disk('public')->putFile("image", $file);
        if (!$filename) {
            return json(["code" => 1, "data" => []], 200);
        }

        $result = [
            "code" => 0,
            "data" => [
                "src" => "/upload/" . $filename,
            ],
        ];
        return json($result, 200);

    }

}
<?php
// 应用公共文件

/**
 * 通用化API数据格式输出
 * @Author   cat
 * @DateTime 2020-05-10T16:55:39+0800
 * @param    [type]                   $status     [description]
 * @param    string                   $message    [description]
 * @param    array                    $data       [description]
 * @param    integer                  $httpStatus [description]
 * @return   [type]                               [description]
 */
function show($status, $message = "error", $data = [], $httpStatus = 200) {
    $result = [
        "status"  => $status,
        "message" => $message,
        "result"  => $data,
    ];

    return json($result, $httpStatus);

}
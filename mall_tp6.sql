/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : mall_tp6

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2020-07-18 11:07:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for mall_admin_user
-- ----------------------------
DROP TABLE IF EXISTS `mall_admin_user`;
CREATE TABLE `mall_admin_user` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '用户表ID',
  `username` varchar(50) DEFAULT '' COMMENT '用户名',
  `password` char(32) DEFAULT '' COMMENT '密码',
  `status` int(11) DEFAULT '0' COMMENT '0待审核 1正常  99逻辑删除',
  `createtime` int(11) DEFAULT '0' COMMENT '用户注册时间',
  `last_login_time` int(11) DEFAULT '0' COMMENT '最后登录时间',
  `ip` varchar(50) DEFAULT '0' COMMENT '登录的IP',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mall_admin_user
-- ----------------------------
INSERT INTO `mall_admin_user` VALUES ('1', 'admin', '123456', '1', '0', '1594651167', '0');

-- ----------------------------
-- Table structure for mall_category
-- ----------------------------
DROP TABLE IF EXISTS `mall_category`;
CREATE TABLE `mall_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '分类名称',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父类ID',
  `icon` varchar(255) NOT NULL DEFAULT '' COMMENT '图标',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '路径 1,2,5',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  `operate_user` varchar(100) NOT NULL DEFAULT '' COMMENT '操作人',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `listorder` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`),
  KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mall_category
-- ----------------------------
INSERT INTO `mall_category` VALUES ('1', '男装', '0', '', '', '1593617215', '1593617215', '', '1', '0');
INSERT INTO `mall_category` VALUES ('2', '女装', '0', '', '', '1593617739', '1593617739', '', '1', '0');
INSERT INTO `mall_category` VALUES ('3', '男鞋', '0', '', '', '1593617916', '1593617916', '', '1', '0');
INSERT INTO `mall_category` VALUES ('4', '女鞋', '0', '', '', '1593617930', '1593617930', '', '1', '0');
INSERT INTO `mall_category` VALUES ('5', '衬衫', '1', '', '', '1593618109', '1593618109', '', '1', '0');
INSERT INTO `mall_category` VALUES ('6', '卫衣', '1', '', '', '1593618130', '1593618130', '', '1', '0');
INSERT INTO `mall_category` VALUES ('7', '上衣', '2', '', '', '1593701125', '1593701125', '', '1', '0');
INSERT INTO `mall_category` VALUES ('8', '裤子', '2', '', '', '1593701140', '1593701140', '', '1', '0');
INSERT INTO `mall_category` VALUES ('9', '西服', '7', '', '', '1593701471', '1593701471', '', '1', '0');
INSERT INTO `mall_category` VALUES ('10', '新款卫衣', '6', '', '', '1593701504', '1593701504', '', '1', '0');
INSERT INTO `mall_category` VALUES ('11', '板鞋', '4', '', '', '1593701548', '1593701548', '', '1', '0');
INSERT INTO `mall_category` VALUES ('12', '高跟鞋', '4', '', '', '1593701563', '1593701563', '', '1', '0');
INSERT INTO `mall_category` VALUES ('13', '皮鞋', '4', '', '', '1593701573', '1593701573', '', '1', '0');
INSERT INTO `mall_category` VALUES ('14', '运动鞋', '3', '', '', '1593701588', '1593701588', '', '1', '0');
INSERT INTO `mall_category` VALUES ('15', '休闲鞋', '3', '', '', '1593701599', '1593701599', '', '1', '0');
INSERT INTO `mall_category` VALUES ('16', '商务鞋', '3', '', '', '1593701613', '1593701613', '', '1', '0');
INSERT INTO `mall_category` VALUES ('17', '休闲裤', '8', '', '', '1594648280', '1594648280', '', '1', '0');
INSERT INTO `mall_category` VALUES ('18', '新潮卫衣', '6', '', '', '1594648343', '1594648343', '', '1', '0');
INSERT INTO `mall_category` VALUES ('19', '商务衬衫', '5', '', '', '1594648448', '1594648448', '', '1', '0');

-- ----------------------------
-- Table structure for mall_goods
-- ----------------------------
DROP TABLE IF EXISTS `mall_goods`;
CREATE TABLE `mall_goods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '商品标题',
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '商品分类',
  `category_path_id` varchar(20) NOT NULL DEFAULT '' COMMENT '栏目ID path',
  `promotion_title` varchar(255) NOT NULL DEFAULT '' COMMENT '商品促销语',
  `goods_unit` varchar(20) NOT NULL DEFAULT '' COMMENT '商品单位',
  `keywords` varchar(100) NOT NULL DEFAULT '' COMMENT '关键词',
  `sub_title` varchar(100) NOT NULL DEFAULT '' COMMENT '副标题',
  `stock` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '库存',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '现价',
  `cost_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '原价',
  `sku_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '商品默认的sku_id',
  `is_show_stock` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否显示库存',
  `production_time` varchar(10) NOT NULL DEFAULT '0' COMMENT '生产日期',
  `goods_specs_type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '商品规则 1统一，2多规格',
  `big_image` varchar(255) NOT NULL DEFAULT '' COMMENT '大图',
  `recommend_image` varchar(255) NOT NULL DEFAULT '' COMMENT '商品推荐图',
  `carousel_image` varchar(500) NOT NULL DEFAULT '' COMMENT '详情页轮播图',
  `description` text NOT NULL COMMENT '商品详情',
  `is_index_recommend` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否首页推荐大图商品',
  `goods_specs_data` varchar(255) NOT NULL DEFAULT '' COMMENT '所有规则属性存放json',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  `operate_user` varchar(255) NOT NULL DEFAULT '',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `listorder` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序字段',
  PRIMARY KEY (`id`),
  KEY `title` (`title`),
  KEY `category_path_id` (`category_path_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mall_goods
-- ----------------------------
INSERT INTO `mall_goods` VALUES ('7', '西服1', '9', '2,7,9', '西服', '件', '西服', '西服', '100', '100.00', '120.00', '1', '1', '2020-07-07', '2', '/upload/image/20200707\\57b6144ac565a74ea30ed1075f2c3e6f.jpg', '/upload/image/20200707\\19e00cd64b2a4f4bbba6635d620e0af9.jpg', '/upload/image/20200707\\2cfecf526a36637250114c2d59856504.jpg,/upload/image/20200707\\7d71a7a9a102666eaa9ed38b45a2cd9c.jpg,/upload/image/20200707\\da02634c4ffe988979f30092aa5d1a0b.jpg', '犬瘟热<img src=\"/upload/image/20200707\\2d2b92f03bac1200f173e7b186e056df.jpg\" alt=\"undefined\">', '1', '', '1594135690', '1594135690', '', '1', '0');
INSERT INTO `mall_goods` VALUES ('8', '恒束 休闲裤男2020夏季新款休闲运动小脚裤男薄款宽松直筒九分裤潮牌百搭工装裤子男装 9085白色 XL', '17', '2,8,17', '恒束 休闲裤男2020夏季新款休闲', '件', '恒束 休闲裤男2020夏季新款休闲', '恒束 休闲裤男2020夏季新款休闲', '100', '100.00', '110.00', '5', '1', '2020-07-01', '2', '/upload/image/20200713\\d832a4812703de0c3ae7f6a999c7cd25.jpg', '/upload/image/20200713\\c9b910bc5eb4f83d78101a652dd2ccd6.jpg', '/upload/image/20200713\\d32f16f778dda93e206a6fffd43643d0.jpg,/upload/image/20200713\\3b965903340cf59c5bbaac3e929005f8.jpg,/upload/image/20200713\\4a4a63e175a82155d05853566dd8e2cd.jpg,/upload/image/20200713\\49d9233f4c13b4ca8fc08c51713a1c9d.jpg,/upload/image/20200713\\f24a495d55c5c4d3b725402026f8c1da.jpg', '<p>你好啊&nbsp; <br></p><p><img src=\"/upload/image/20200713\\5522170dc6a55453a6e4dee11dbd21bd.jpg\" alt=\"undefined\"><br></p>', '0', '', '1594651293', '1594651293', '', '1', '0');
INSERT INTO `mall_goods` VALUES ('9', ' 休闲裤男2020夏季新款休闲运动小脚裤男薄款宽松直筒九分裤潮牌百搭工装裤子男装', '17', '2,8,17', '休闲裤男2020夏季新款休闲', '件', '休闲裤男2020夏季新款休闲', '休闲裤男2020夏季新款休闲', '100', '90.00', '90.00', '9', '1', '2020-07-01', '2', '/upload/image/20200713\\a5e195eb40aeade58ad3af3e7318d835.jpg', '/upload/image/20200713\\b8b0ef13ac127411b571c09fb5fc921d.jpg', '/upload/image/20200713\\94a2adee7bb6cd758031db68d73aa936.jpg,/upload/image/20200713\\01266169efa6bd74cb90e81986187981.jpg,/upload/image/20200713\\5455227479ddd3afbc30376c3258d56c.jpg,/upload/image/20200713\\50855c2e82e4d3b2f3769aa13b2ac1a1.jpg,/upload/image/20200713\\9e2cf56e5db37b159009a090ba703c74.jpg', '<p>发的</p><p><img src=\"/upload/image/20200713\\2ab7e4dcdd009b36907f0c67fe9b9e96.jpg\" alt=\"undefined\"><br></p>', '0', '', '1594651748', '1594651749', '', '1', '0');

-- ----------------------------
-- Table structure for mall_goods_sku
-- ----------------------------
DROP TABLE IF EXISTS `mall_goods_sku`;
CREATE TABLE `mall_goods_sku` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `goods_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '商品Id',
  `specs_value_ids` varchar(255) NOT NULL COMMENT '每行规则属性ID 按逗号连接',
  `price` decimal(10,2) unsigned NOT NULL COMMENT '现价',
  `cost_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '原价',
  `stock` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '库存',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `goods_id` (`goods_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mall_goods_sku
-- ----------------------------
INSERT INTO `mall_goods_sku` VALUES ('1', '7', '1,5', '100.00', '120.00', '20', '1', '1594135690', '1594135690');
INSERT INTO `mall_goods_sku` VALUES ('2', '7', '1,6', '120.00', '110.00', '30', '1', '1594135690', '1594135690');
INSERT INTO `mall_goods_sku` VALUES ('3', '7', '2,5', '110.00', '120.00', '40', '1', '1594135690', '1594135690');
INSERT INTO `mall_goods_sku` VALUES ('4', '7', '2,6', '130.00', '110.00', '10', '1', '1594135690', '1594135690');
INSERT INTO `mall_goods_sku` VALUES ('5', '8', '1,5', '100.00', '110.00', '10', '1', '1594651293', '1594651293');
INSERT INTO `mall_goods_sku` VALUES ('6', '8', '1,6', '100.00', '110.00', '20', '1', '1594651293', '1594651293');
INSERT INTO `mall_goods_sku` VALUES ('7', '8', '22,5', '100.00', '110.00', '30', '1', '1594651293', '1594651293');
INSERT INTO `mall_goods_sku` VALUES ('8', '8', '22,6', '100.00', '110.00', '40', '1', '1594651293', '1594651293');
INSERT INTO `mall_goods_sku` VALUES ('9', '9', '1,5', '90.00', '90.00', '20', '1', '1594651749', '1594651749');
INSERT INTO `mall_goods_sku` VALUES ('10', '9', '1,7', '90.00', '90.00', '40', '1', '1594651749', '1594651749');
INSERT INTO `mall_goods_sku` VALUES ('11', '9', '22,5', '90.00', '90.00', '30', '1', '1594651749', '1594651749');
INSERT INTO `mall_goods_sku` VALUES ('12', '9', '22,7', '90.00', '90.00', '10', '1', '1594651749', '1594651749');

-- ----------------------------
-- Table structure for mall_order
-- ----------------------------
DROP TABLE IF EXISTS `mall_order`;
CREATE TABLE `mall_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(10) NOT NULL DEFAULT '0' COMMENT '用户id',
  `order_id` varchar(50) NOT NULL DEFAULT '' COMMENT '订单ID',
  `total_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '支付金额',
  `pay_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '支付方式',
  `logistics` varchar(20) NOT NULL DEFAULT '' COMMENT '物流名称',
  `logistics_order` varchar(50) NOT NULL DEFAULT '' COMMENT '物流订单号',
  `message` varchar(200) NOT NULL DEFAULT '' COMMENT '购买者留言信息',
  `address_id` int(10) NOT NULL DEFAULT '0' COMMENT '地址ID',
  `create_time` int(10) NOT NULL DEFAULT '0',
  `update_time` int(10) NOT NULL DEFAULT '0',
  `pay_time` int(10) NOT NULL DEFAULT '0',
  `consignment_ti` int(10) NOT NULL DEFAULT '0' COMMENT '发货时间',
  `end_time` int(10) NOT NULL DEFAULT '0' COMMENT '交易完成时间',
  `close_time` int(10) NOT NULL DEFAULT '0' COMMENT '交易关闭时间',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1待支付  2已支付  3已发货  4已收货  5 已完成 6 退款退货 7已取消',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`,`order_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mall_order
-- ----------------------------

-- ----------------------------
-- Table structure for mall_order_goods
-- ----------------------------
DROP TABLE IF EXISTS `mall_order_goods`;
CREATE TABLE `mall_order_goods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` varchar(100) NOT NULL DEFAULT '' COMMENT '订单ID',
  `sku_id` int(10) NOT NULL,
  `goods_id` int(10) NOT NULL,
  `num` int(10) NOT NULL DEFAULT '0' COMMENT '数量',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '商品价格',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '商品标题',
  `image` varchar(255) NOT NULL DEFAULT '' COMMENT '商品图片',
  `create_time` int(10) NOT NULL DEFAULT '0',
  `update_time` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mall_order_goods
-- ----------------------------
INSERT INTO `mall_order_goods` VALUES ('2', '0', '12', '9', '2', '90.00', ' 休闲裤男2020夏季新款休闲运动小脚裤男薄款宽松直筒九分裤潮牌百搭工装裤子男装', 'http://www.tp6.0.com/upload/image/20200713\\b8b0ef13ac127411b571c09fb5fc921d.jpg', '1594740140', '1594825636');

-- ----------------------------
-- Table structure for mall_specs_value
-- ----------------------------
DROP TABLE IF EXISTS `mall_specs_value`;
CREATE TABLE `mall_specs_value` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `specs_id` int(10) unsigned NOT NULL COMMENT '规格ID',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '规格属性名',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  `operate_user` varchar(100) NOT NULL DEFAULT '',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `specs_id` (`specs_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mall_specs_value
-- ----------------------------
INSERT INTO `mall_specs_value` VALUES ('1', '1', '黄色', '1593703478', '1593703478', '', '1');
INSERT INTO `mall_specs_value` VALUES ('2', '1', '白色', '1593703482', '1593703482', '', '1');
INSERT INTO `mall_specs_value` VALUES ('3', '1', '灰色', '1593703487', '1593703487', '', '1');
INSERT INTO `mall_specs_value` VALUES ('4', '1', '黑色', '1593703489', '1593703489', '', '1');
INSERT INTO `mall_specs_value` VALUES ('5', '2', 'M', '1593703553', '1593703553', '', '1');
INSERT INTO `mall_specs_value` VALUES ('6', '2', 'L', '1593703554', '1593703554', '', '1');
INSERT INTO `mall_specs_value` VALUES ('7', '2', 'XL', '1593703558', '1593703558', '', '1');
INSERT INTO `mall_specs_value` VALUES ('8', '2', 'XXL', '1593703561', '1593703561', '', '1');
INSERT INTO `mall_specs_value` VALUES ('9', '2', 'XXXL', '1593703590', '1593703590', '', '1');
INSERT INTO `mall_specs_value` VALUES ('10', '4', '40', '1593703616', '1593703616', '', '1');
INSERT INTO `mall_specs_value` VALUES ('11', '4', '38', '1593703626', '1593703626', '', '1');
INSERT INTO `mall_specs_value` VALUES ('12', '4', '39', '1593703629', '1593703629', '', '1');
INSERT INTO `mall_specs_value` VALUES ('13', '4', '40', '1593703631', '1593703631', '', '1');
INSERT INTO `mall_specs_value` VALUES ('14', '4', '41', '1593703633', '1593703633', '', '1');
INSERT INTO `mall_specs_value` VALUES ('15', '4', '42', '1593703635', '1593703635', '', '1');
INSERT INTO `mall_specs_value` VALUES ('16', '4', '43', '1593703637', '1593703637', '', '1');
INSERT INTO `mall_specs_value` VALUES ('17', '4', '43', '1593703638', '1593703638', '', '1');
INSERT INTO `mall_specs_value` VALUES ('18', '3', '64G', '1593703647', '1593703647', '', '1');
INSERT INTO `mall_specs_value` VALUES ('19', '3', '128G', '1593703652', '1593703652', '', '1');
INSERT INTO `mall_specs_value` VALUES ('20', '3', '256G', '1593703662', '1593703662', '', '1');
INSERT INTO `mall_specs_value` VALUES ('21', '2', 'M', '1593949555', '1593949555', '', '1');
INSERT INTO `mall_specs_value` VALUES ('22', '1', '红色', '1594648772', '1594648772', '', '1');

-- ----------------------------
-- Table structure for mall_user
-- ----------------------------
DROP TABLE IF EXISTS `mall_user`;
CREATE TABLE `mall_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `username` varchar(100) NOT NULL DEFAULT '' COMMENT '用户名',
  `phone_number` varchar(20) NOT NULL COMMENT '手机号',
  `password` char(32) NOT NULL DEFAULT '',
  `ltype` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '登录方式 默认0 手机号码登录 1用户名密码登录',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '会话保存天数',
  `sex` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '性别',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `operate_user` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `usernme` (`username`),
  KEY `phone_number` (`phone_number`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mall_user
-- ----------------------------
INSERT INTO `mall_user` VALUES ('1', 'singwa粉-15209670092', '15209670092', '', '0', '2', '0', '1594483992', '1594483992', '1', '');
